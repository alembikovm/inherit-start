// Задания:
// 1. Добавить обработку toggle - на кнопку, чтобы ВСЕ  widget-ы открывались закрывались (есть в css класс close для этого) 
// 2. Создать на основе Widget функцию конструктор для picture добавить её шаблон внутрь (см пример текст), установить нужной src
// 3. Создать на основе Widget функцию конструктор для counter реализовать возможность инкремента, декремента с отображением соответсвующего значения
// вопросы в группу в skype


var widgets = [
    {
        id: 'text',
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, enim?'
    },
    {
        id: 'picture',
        src: 'img/kitten.jpg',
        width: 200,
        height: 150
    },
    {
        id: 'counter',
        value: 3
    }
];

// скобки обратные - фишка es6
var widgetTemplate = `
<div class="widget__preview">
    Block
</div>
<div class="widget__header">
    <button class="toggle-button">toggle</button>
</div>
<div class="widget__body">
    Body
</div>
`;

function Widget(option) {
    var id = option.id;

    this.el = document.createElement('div');
    // добавляем в  this.el html-разметку, которая есть в шаблонах.
    this.el.innerHTML = widgetTemplate;
    this.el.id = id;

    // чтобы работать с полученной разметкой необходимо в ней (this.el) найти нужную dom-node - делается это с помощью querySelector
    this.preview = this.el.querySelector('.widget__preview');
    this.preview.innerText = id;

    // сохраняем в this.container ссылку на <div class="widget__body"> - чтобы в потомках, можно было добавлять их шаблон в него и не искать каждый раз заново. 
    this.container = this.el.querySelector('.widget__body');

    this.el.classList.add('widget');
    this.el.setAttribute('draggable', true);

    // binds
    var handleDrag = this.handleDrag.bind(this);

    //handlers
    this.el.ondragstart = handleDrag;
}

Widget.prototype.handleDrag = function(event) {
    event.dataTransfer.setData('text/plain', event.currentTarget.id)
};


var counterTemplate = `
<div class="counter">
    <div class="counter__display">1</div>
    <div class="counter__control">
        <button class="decrement">-</button>
        <button class="increment">+</button>
    </div>
</div>
`;

var imageTemplate = `
<img class="image" src="" alt="">
`;

var textTemplate = `
<p class="text"></p>
`;
// Функцию-конструктор наследует Widget, и добавляет в него новые возможности.
function Text(option) {
    // вызов функции-конструктора родителя
    Widget.call(this, option);
    this.container.innerHTML = textTemplate;
    this.container.querySelector('.text').innerText = option.content;
}

Text.prototype = Object.create(Widget.prototype);
Text.prototype.constructor = Text;



function Droppable(selector) {

    // это пример включенного наследования
    // this.el содержится (has_a) в полученном объекте.
    // так можно использовать его при необходимости сзаимодействия с dom
    this.el = document.querySelector(selector);
    
    // делается чтобы сохранить контекст функции на this
    // так в обработчиках будет доступна переменная this
    // указывающая на создаваемый объект
    // this.handleDragEnter - определяются нижу в прототипе
    // binds
    var handleDragEnter = this.handleDragEnter.bind(this);
    var handleDrop = this.handleDrop.bind(this);
    var handleDragLeave = this.handleDragLeave.bind(this);

    // подписываемся на события
    // handlers
    this.el.ondragover = handleDragEnter;
    this.el.ondrop = handleDrop;
    this.el.ondragleave = handleDragLeave;
}

Droppable.prototype.add = function(widget) {
    this.el.appendChild(widget.el);
}

Droppable.prototype.handleDrop = function(event) {
    var targetId = event.dataTransfer.getData("text");

    var dropped = document.getElementById(targetId);
    if (!dropped) return false;

    event.currentTarget.appendChild(dropped);
    this.el.style.border = 'none';
    return false;
}

Droppable.prototype.handleDragEnter = function() {
    this.el.style.border = '4px solid lightgreen';
    return false;
}

Droppable.prototype.handleDragLeave = function() {
    this.el.style.border = 'none';  
}

// Функция для создания объектов - widgets-ов из массива
// сюда нужно добавить соответсвующие case после того как
// cсоздатите их конструкторы
function createWigets(options) {
    return options.map(function(option) {
        var widget;
        switch (option.id.toLowerCase()) {
            case 'text':
                widget = new Text(option);
                break;

            default: widget = new Widget(option);
        }
        return widget;
    });
}

// добавляем все widgets на панели
function addWigets(panel, widgets) {
    widgets.forEach(function(widget) {
        panel.add(widget);
    });
}

// создаем панели
var controlPanel = new Droppable('#controlPanel');
var widgetsPanel = new Droppable('#widgetsPanel');

// вызываем функцию для добавления widgets на панель
addWigets(widgetsPanel, createWigets(widgets));




