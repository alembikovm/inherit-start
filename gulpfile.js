var gulp = require('gulp');
var del = require('del');
var sass = require('gulp-sass');
var notify = require('gulp-notify');
var bs = require('browser-sync').create();
var sm = require('gulp-sourcemaps');
var concat = require('gulp-concat');

gulp.task('html', function () {
    return gulp.src('./src/index.html')
        .pipe(gulp.dest('./build'))
        .pipe(bs.stream());
});

gulp.task('js', function () {
    return gulp.src('./src/js/**/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./build'))
        .pipe(bs.stream());
});

gulp.task('sass', ['html'] ,function () {
    return gulp.src('./src/sass/main.scss')
        .pipe(sm.init())
        .pipe(sass().on('error', notify.onError({title: 'sass'})))
        .pipe(sm.write('.'))
        .pipe(gulp.dest('./build'))
        .pipe(bs.stream());
});

gulp.task('assets', function () {
    return gulp.src('./src/**/*.{ttf,png,jpeg,jpg}')
        .pipe(gulp.dest('./build'));
});

gulp.task('clean', function () {
    return del('./build/*');
});

gulp.task('server', function () {
    bs.init({
        server: {
            baseDir: "./build"
        }
    });

    gulp.watch('./build/index.html').on('change', bs.reload);
});

gulp.task('watch', function () {
    gulp.watch('./src/*.html', ['html']);
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['js']);
    gulp.watch('./src/**/*.{ttf,png,jpeg,jpg}', ['assets']);
});

gulp.task('default', ['html', 'sass', 'js', 'assets', 'server', 'watch']);